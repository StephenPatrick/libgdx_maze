package com.scythes.maze;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.IntIntMap;
import com.scythes.maze.grid.GridLocation;

import java.util.ArrayList;

/** Takes a {@link Camera} instance and controls it via w,a,s,d and mouse panning.
 * @author badlogic */
public class Player extends InputAdapter {
    private final Camera camera;
    private final IntIntMap keys = new IntIntMap();
    private final static int STRAFE_LEFT = Keys.A;
    private final static int STRAFE_RIGHT = Keys.D;
    private final static int FORWARD = Keys.W;
    private final static int BACKWARD = Keys.S;
    private final static int ROTATE_LEFT = Keys.Q;
    private final static int ROTATE_RIGHT = Keys.E;
    private final static int RUN = Keys.SHIFT_LEFT;
    private float velocity = 1f/6;
    private float rotateVelocity = 3;
    private final Vector3 tmp = new Vector3();
    private int moveTime = 0;
    private double deltaX = 0;
    private int locX;
    private int locZ;
    private Direction dir;
    private ArrayList<ArrayList<GridLocation>> grid;
    private MazeGame game;

    public Player(int x, int z, MazeGame mazeGame) {
        camera = new PerspectiveCamera(67, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        camera.lookAt(0,0,0);
        camera.near = 1f;
        camera.far = 300f;
        camera.translate(x*5,0,z*5);
        camera.update();

        locX = x;
        locZ = z;
        dir = Direction.SOUTH;

        grid = mazeGame.grid;
        game = mazeGame;
    }

    @Override
    public boolean keyDown (int keycode) {
        keys.put(keycode, keycode);
        return true;
    }

    @Override
    public boolean keyUp (int keycode) {
        keys.remove(keycode, 0);
        return true;
    }

    public Camera getCamera(){
        return camera;
    }

    public boolean changeGridPosition(int movement) {
        int changeX;
        int changeZ;
        int changeForward = 0;
        int changeSide = 0;

        // Give a value to the relative change the movement represents
        // Todo: make this an enum like direction
        switch (movement) {
            case FORWARD: changeForward = 1; break;
            case BACKWARD: changeForward = -1; break;
            case STRAFE_LEFT: changeSide = 1; break;
            case STRAFE_RIGHT: changeSide = -1; break;
        }

        // Convert that relative change to an absolute change based on direction
        if (dir == Direction.SOUTH) {
            changeZ = changeForward * -1;
            changeX = changeSide * -1;
        } else if (dir == Direction.EAST) {
            changeZ = changeSide;
            changeX = changeForward*-1;
        } else if (dir == Direction.WEST) {
            changeZ = changeSide*-1;
            changeX = changeForward;
        } else {
            changeZ = changeForward;
            changeX = changeSide;
        }
        // Try to move to a new space
        locX += changeX;
        locZ += changeZ;

        // Revert that movement if that space is taken
        if (spaceOccupied(locZ,locX)){
            locX -= changeX;
            locZ -= changeZ;
            moveTime = 1; // Stop movement briefly
            return false;
        }
        // Return the movement was successful
        return true;
    }

    // Wrap around
    public boolean spaceOccupied(int z, int x) {
        if (z < 0 || x < 0) {
            return false;
        }
        return grid.size() > z && grid.get(z).size() > x && grid.get(z).get(x).isOccupied();
    }

    public boolean exitSpace(int z, int x) {
        if (z < 0 || x < 0) {
            return false;
        }
        return grid.size() > z && grid.get(z).size() > x && grid.get(z).get(x).isExit();
    }

    public void update () {
        // Reset movement variables when you run out of time to move.
        if (moveTime == 0) {
            moveTime--;
            tmp.set(0,0,0);
            deltaX = 0;
        }
        // If available to move, accept new inputs for movement.
        else if (moveTime == -1) {
            int velMod = 1;
            int moveLength = 30;
            if (keys.containsKey(RUN)) {
                moveLength = 15;
                velMod = 2;
            }
            tmp.set(0,0,0);
            moveTime = moveLength;
            deltaX = 0;
            // Switch on input keys
            if (keys.containsKey(FORWARD)) {
                // If we're allowed to move,
                if (changeGridPosition(FORWARD)) {
                    // Move the camera in our facing direction appropriately.
                    tmp.set(camera.direction).nor().scl(velocity * velMod);
                }
            } else if (keys.containsKey(BACKWARD)) {
                if (changeGridPosition(BACKWARD)) {
                    tmp.set(camera.direction).nor().scl(-velocity * velMod);
                }
            } else if (keys.containsKey(STRAFE_LEFT)) {
                if (changeGridPosition(STRAFE_LEFT)) {
                    tmp.set(camera.direction).crs(camera.up).nor().scl(-velocity * velMod);
                }
            } else if (keys.containsKey(STRAFE_RIGHT)) {
                if (changeGridPosition(STRAFE_RIGHT)) {
                    tmp.set(camera.direction).crs(camera.up).nor().scl(velocity * velMod);
                }
            } else if (keys.containsKey(ROTATE_LEFT)) {
                deltaX = rotateVelocity*velMod;
                dir = dir.left;
            } else if (keys.containsKey(ROTATE_RIGHT)) {
                deltaX = -1*rotateVelocity*velMod;
                dir = dir.right;
            } else {
                moveTime = -1;
            }
        // When already moving, continue to move.
        } else {
            game.endStart();
            camera.direction.rotate(camera.up, (float)deltaX);
            this.camera.position.add(tmp);
            moveTime--;
        }
        // This is a barebones collision check.
        // When you're in the exit tile's space, inform the game.
        if (exitSpace(locZ,locX)) {
            game.endGame();
        }
        camera.update(true);
    }
}
