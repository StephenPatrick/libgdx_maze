package com.scythes.maze.grid;

import com.badlogic.gdx.graphics.g3d.ModelInstance;

/**
 * A part of the grid which has a model in it's position.
 */
public class GridModel extends GridLocation {

    ModelInstance instance;

    public GridModel(ModelInstance instance) {
        this.instance = instance;
    }

    @Override
    public boolean isOccupied() {
        return true;
    }

    public ModelInstance getInstance(){
        return instance;
    }
}
