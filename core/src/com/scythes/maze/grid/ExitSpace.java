package com.scythes.maze.grid;

import com.badlogic.gdx.graphics.g3d.ModelInstance;

public class ExitSpace extends GridModel {

    public ExitSpace(ModelInstance instance) {
        super(instance);
    }

    @Override
    public boolean isExit() {
        return true;
    }

    @Override
    public boolean isOccupied() {
        return false;
    }
}
