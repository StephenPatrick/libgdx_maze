package com.scythes.maze.grid;

public abstract class GridLocation {

    /**
     * Is this grid location occupied by something?
     * @return
     */
    public boolean isOccupied() {
        return false;
    }

    /**
     * Is this grid location an exit to the maze?
     * @return
     */
    public boolean isExit() {
        return false;
    }
}
