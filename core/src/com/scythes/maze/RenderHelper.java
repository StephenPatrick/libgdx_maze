package com.scythes.maze;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.math.Vector3;

/**
 * Helper functions for rendering
 *
 * Todo: Important performance improvement-- parse all files loaded and store textures loaded.
 * This is to disallow loading the same source files multiple times.
 */
public class RenderHelper {

    private ModelBuilder modelBuilder;
    final private float def_w = 5f;
    final private float def_h = 5f;
    final private float def_d = 5f;
    final private int def_repeating_w = 1028*32;
    final private int def_repeating_h = 1028*32;
    final private float def_plane_h = 1f;

    public RenderHelper() {
        modelBuilder = new ModelBuilder();
    }

    /**
     * Create a repeating texture, given an existing texture. Will be applied
     * to default dimensions.
     *
     * @param texture
     * @return
     */
    public TextureRegion createRepeatingTexture(Texture texture) {
        texture.setWrap(Texture.TextureWrap.Repeat,Texture.TextureWrap.Repeat);
        TextureRegion region = new TextureRegion(texture);
        region.setRegion(0,0,def_repeating_w,def_repeating_h);
        return region;
    }

    /**
     * Create a repeating texture, given a filename.
     *
     * @param file
     * @return
     */
    public TextureRegion createRepeatingTexture(String file) {
        Texture texture = createTexture(file);
        texture.setWrap(Texture.TextureWrap.Repeat,Texture.TextureWrap.Repeat);
        TextureRegion region = new TextureRegion(texture);
        region.setRegion(0,0,def_repeating_w,def_repeating_h);
        return region;
    }

    /**
     * Create a Box, given a file and dimensions.
     *
     * @param file
     * @param w
     * @param h
     * @param d
     * @return
     */
    public Model createBox(String file, float w, float h, float d){
        return createBox(createTexture(file),w,h,d);
    }

    /**
     * Create a box, given a file. Will be applied to default dimensions.
     *
     * @param file
     * @return
     */
    public Model createBox(String file) {
        return createBox(createTexture(file), def_w, def_h, def_d);
    }

    public Model createBox(Color color, float w, float h, float d) {
        return modelBuilder.createBox(5f, 5f, 5f,
                new Material(ColorAttribute.createDiffuse(color)),
                VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal);
    }

    public Model createBox(Color color) {
        return createBox(color, def_w, def_h, def_d);
    }

    /**
     * Create a box, given an existing texture and dimensions.
     * @param texture
     * @param w
     * @param h
     * @param d
     * @return
     */
    public Model createBox(Texture texture, float w, float h, float d) {
        return modelBuilder.createBox(w, h, d,
                new Material(new TextureAttribute(TextureAttribute.Diffuse, texture)),
                VertexAttributes.Usage.Position |
                VertexAttributes.Usage.Normal |
                VertexAttributes.Usage.TextureCoordinates);
    }

    public Model createBox(TextureRegion texture, float w, float h, float d) {
        return modelBuilder.createBox(w, h, d,
                new Material(new TextureAttribute(TextureAttribute.Diffuse, texture)),
                VertexAttributes.Usage.Position |
                VertexAttributes.Usage.Normal |
                VertexAttributes.Usage.TextureCoordinates);
    }

    /**
     * Createa a box, given an existing texture. Will be applied to default dimensions.
     * @param texture
     * @return
     */
    public Model createBox(Texture texture){
        return createBox(texture, def_w, def_h, def_d);
    }

    /**
     * Create a texture, given a filename.
     *
     * @param file
     * @return
     */
    public Texture createTexture(String file){
        return new Texture(Gdx.files.local("core/assets/"+file),true);
    }

    /**
     * Create a generic ambient environment
     * @return
     */
    public Environment createAmbientEnvironment() {
        Environment environment = new Environment();
        environment.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.4f, 0.4f, 0.4f, 1f));
        environment.add(new DirectionalLight().set(0.8f, 0.8f, 0.8f, -1f, -0.8f, -0.2f));
        return environment;
    }

    /**
     * Create a plane, given a width, depth, and texture.
     * @param w
     * @param d
     * @param region
     * @return
     */
    public Model makePlane(float w, float d, TextureRegion region) {
        return createBox(region, w, def_plane_h, d);
    }

    /**
     * Create a plane as an instance, given position, dimensions, and texture.
     * @param x
     * @param y
     * @param z
     * @param w
     * @param d
     * @param region
     * @return
     */
    public ModelInstance makePlaneInstance(int x, float y, int z, float w, float d, TextureRegion region){
        // "Rectangles" take 12 undocumented arguments so we aren't using them
        // Instead we make a really short box
        return new ModelInstance(makePlane(w, d, region), new Vector3(x*5,y,z*5));
    }
}

