package com.scythes.maze;

import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.ThreadLocalRandom;

public class MazeGenerationHelper {

    /**
     * Create a maze which is wrapped by a layer of empty grid spaces.
     *
     * @param gridWidth
     * @param gridHeight
     * @return
     */
    public static ArrayList<ArrayList<Boolean>> basicInnerMaze(int gridWidth, int gridHeight) {

        ArrayList<ArrayList<Boolean>> wallGrid = initGrid(gridWidth,gridHeight);

        int failures = 0;
        while (failures < 30) {
            int x = ThreadLocalRandom.current().nextInt(1, gridWidth - 1);
            int z = ThreadLocalRandom.current().nextInt(1, gridHeight - 1);
            int bordering = 0;
            for (int xMod = -1; xMod < 2; xMod += 1) {
                for (int zMod = -1; zMod < 2; zMod += 1) {
                    if (wallGrid.get(z + zMod).get(x + xMod)) {
                        bordering++;
                    }
                }
            }
            // This allows walls to appear out of nowhere, not just from other walls.
            if (bordering < 3) {
                wallGrid.get(z).set(x, true);
                failures = 0;
            } else {
                failures++;
            }
        }
        return wallGrid;
    }

    /**
     * Create a maze where all walls connect to the border of the map.
     *
     * @param gridWidth
     * @param gridHeight
     * @return
     */
    public static ArrayList<ArrayList<Boolean>> basicMaze(int gridWidth, int gridHeight){

        ArrayList<ArrayList<Boolean>> wallGrid = initGrid(gridWidth,gridHeight);

        int failures = 0;
        while (failures < 100) {
            int x = ThreadLocalRandom.current().nextInt(1, gridWidth - 1);
            int z = ThreadLocalRandom.current().nextInt(1, gridHeight - 1);
            int bordering = 0;
            for (int xMod = -1; xMod < 2; xMod += 1) {
                for (int zMod = -1; zMod < 2; zMod += 1) {
                    if (wallGrid.get(z + zMod).get(x + xMod)) {
                        bordering++;
                    }
                }
            }
            // This demands that all walls come out of other walls
            if (bordering == 1 || ((x == 1 || x == gridWidth-2 || z == 1 || z == gridHeight-2)
                    && bordering == 3)) {
                wallGrid.get(z).set(x, true);
                failures = 0;
            } else {
                failures++;
            }
        }
        return wallGrid;
    }

    // Create a base maze grid (empty, with a border wall)
    private static ArrayList<ArrayList<Boolean>> initGrid(int gridWidth, int gridHeight) {

        ArrayList<ArrayList<Boolean>> wallGrid = new ArrayList<>();

        for (int i = 0; i < gridHeight; i++) {
            ArrayList<Boolean> sublist = new ArrayList<>();
            // All nodes are blank to start
            for (int j = 0; j < gridWidth; j++) {
                sublist.add(false);
            }
            // There is an exterior border to the maze.
            sublist.set(0, true);
            sublist.set(gridWidth - 1, true);

            wallGrid.add(sublist);
        }
        // There is still an exterior border to the maze.
        Collections.fill(wallGrid.get(0), Boolean.TRUE);
        Collections.fill(wallGrid.get(wallGrid.size() - 1), Boolean.TRUE);

        return wallGrid;
    }
}
