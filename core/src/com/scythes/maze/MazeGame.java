package com.scythes.maze;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.math.Vector3;
import com.scythes.maze.grid.EmptyGridLocation;
import com.scythes.maze.grid.ExitSpace;
import com.scythes.maze.grid.GridLocation;
import com.scythes.maze.grid.GridModel;

import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

// Main class.
// Is the game.
public class MazeGame extends ApplicationAdapter {

	// Builders / Batches
	public ModelBuilder modelBuilder;
	public ModelBatch modelBatch;
	public SpriteBatch spriteBatch;
	public BitmapFont font;

    public RenderHelper rh;

	// Unique entities
	public ModelInstance ground;
	public ModelInstance ceiling;
	public Environment environment;
	public Player player;
    public Model wall;
    public Model exit;

	// grid control
	public ArrayList<ArrayList<GridLocation>> grid;
	final private int gridWidth = 30;
	final private int gridHeight = 30;

	// Game sequence control
	private boolean gameComplete = false;
	private boolean gameAtStart = true;


	@Override
	public void create () {


        // Batch / Builder setup

		spriteBatch = new SpriteBatch();
		font = new BitmapFont();
		font.setColor(Color.WHITE);

		modelBatch = new ModelBatch();
		modelBuilder = new ModelBuilder();

        rh = new RenderHelper();

        Texture cobble = new Texture(Gdx.files.local("core/assets/brick.png"), true);
        TextureRegion cobbleRegion = rh.createRepeatingTexture(cobble);
        TextureRegion woodRegion = rh.createRepeatingTexture("wood.png");

        wall = rh.createBox(cobble);
        exit = rh.createBox(Color.GOLD);

        // Make the models in the environment visible by pretending there's a light somewhere

		environment = rh.createAmbientEnvironment();

        // These variables will need to change should the grid size change.

		ground = rh.makePlaneInstance(0,-3f,0,300,300,woodRegion);
		ceiling = rh.makePlaneInstance(0, 3f, 0, 300, 300, cobbleRegion);

        // Generate a grid to base the maze off of.

		ArrayList<ArrayList<Boolean>> wallGrid;
		if (Math.random() < 0.5) {
			wallGrid = MazeGenerationHelper.basicMaze(gridWidth, gridHeight);
		} else {
			wallGrid = MazeGenerationHelper.basicInnerMaze(gridWidth, gridHeight);
		}

		// Find a location to place the exit
        // This algorithm is O(infinity)

		int exitX;
		int exitZ;

		while (true) {
			int x = ThreadLocalRandom.current().nextInt(4, gridWidth - 4);
			int z = ThreadLocalRandom.current().nextInt(4, gridHeight - 4);
			if (!wallGrid.get(z).get(x)) {
				exitX = x;
				exitZ = z;
				break;
			}
		}

		// Initialize all blocks inside the maze

		grid = new ArrayList<>();
		for(int z = 0; z < gridHeight; z++) {
			ArrayList<GridLocation> gridLine = new ArrayList<>();
			for (int x = 0; x < gridWidth; x++){
				boolean addBlock = wallGrid.get(z).get(x);
				if (addBlock) {
					gridLine.add(makeWall(x, z));
				} else {
					gridLine.add(new EmptyGridLocation());
				}
			}
			grid.add(gridLine);
		}
		grid.get(exitZ).set(exitX, makeExit(exitX,exitZ));

		// Setup the player

		player = new Player(1,1,this);
		Gdx.input.setInputProcessor(player);
	}

    public GridModel makeWall(int x, int z) {
        return new GridModel(new ModelInstance(wall, new Vector3(x*5,0,z*5)));
    }
    public ExitSpace makeExit(int x, int z){
        return new ExitSpace(new ModelInstance(exit, new Vector3(x*5,0,z*5)));
    }

	public void endGame() {
		gameComplete = true;
	}

	public void endStart() {
		gameAtStart = false;
	}

	@Override
	public void render () {

		player.update();

		// Clear the previous screen

		Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

		// Render models

		modelBatch.begin(player.getCamera());
		for (ArrayList<GridLocation> subArray : grid) {
            subArray.forEach(loc -> {
                if (loc instanceof GridModel) {
                    modelBatch.render(((GridModel) loc).getInstance(), environment);
                }
            });
		}
		modelBatch.render(ceiling, environment);
		modelBatch.render(ground, environment);

		modelBatch.end();

		// Render text

		if (gameComplete) {
			spriteBatch.begin();
			font.draw(spriteBatch, "You found the exit!", Gdx.graphics.getWidth()/2, 100);
			spriteBatch.end();
		} else if (gameAtStart) {
			spriteBatch.begin();
			font.draw(spriteBatch, "You're in a maze! The exit is a gold block!",
					100, 140);
			font.draw(spriteBatch, "WASD to Move! QE to Rotate! Shift for Double Speed!",
					100, 100);
			spriteBatch.end();
		}
	}

	@Override
	public void dispose () {
	}

	@Override
	public void resume () {
	}

	@Override
	public void resize (int width, int height) {
	}

	@Override
	public void pause () {
	}
}
