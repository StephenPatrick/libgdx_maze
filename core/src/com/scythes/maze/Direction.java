package com.scythes.maze;

public enum Direction {
    NORTH,SOUTH,EAST,WEST;

    public Direction left;
    public Direction right;

    static {
        NORTH.left = WEST;
        NORTH.right = EAST;
        EAST.left = NORTH;
        EAST.right = SOUTH;
        WEST.left = SOUTH;
        WEST.right = NORTH;
        SOUTH.left = EAST;
        SOUTH.right = WEST;
    }
}
